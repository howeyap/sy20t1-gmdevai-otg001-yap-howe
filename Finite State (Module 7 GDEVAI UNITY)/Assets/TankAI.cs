﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankAI : Unit
{
    Animator anim;

    public GameObject player;

    private Health myHealth;

    public GameObject GetPlayer()
    {
        return player;
    }

    private void Start()
    {
        myHealth = GetComponent<Health>();
        anim = this.GetComponent<Animator>();
    }

    private void Update()
    {
        anim.SetFloat("distance", Vector3.Distance(transform.position, player.transform.position));
        anim.SetInteger("health", myHealth.GetCurrentHealth());
    }
}
