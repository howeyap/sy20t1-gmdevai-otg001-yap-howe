﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

	public GameObject explosion;

	private int damage;
	
	void OnCollisionEnter(Collision col)
    {
        if (col.collider.CompareTag("Tank"))
        {
            col.collider.GetComponent<Health>().TakeDamage(damage);
        }
    	GameObject e = Instantiate(explosion, this.transform.position, Quaternion.identity);
    	Destroy(e,1.5f);
    	Destroy(this.gameObject);
    }

    public void SetDamage(int dmg)
    {
        damage = dmg;
    }
}
