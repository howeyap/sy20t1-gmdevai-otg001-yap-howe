﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class HealthBar : MonoBehaviour
{
    private Image hpBar;

    private float currentFill;

    [SerializeField]
    private Health playerHealth;

    private void Awake()
    {
        hpBar = GetComponent<Image>();
        hpBar.fillAmount = (float)playerHealth.GetCurrentHealth() / (float)playerHealth.GetMaxHealth();
    }
    private void Update()
    {
        currentFill = (float)playerHealth.GetCurrentHealth() / (float)playerHealth.GetMaxHealth();
        hpBar.fillAmount = currentFill;
        //hpBar.fillAmount = Mathf.Lerp(hpBar.fillAmount, currentFill, Time.deltaTime * 5);

        if (playerHealth.GetCurrentHealth() <= 0)
        {
            StartCoroutine(Reset());
        }
    }

    private IEnumerator Reset()
    {
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
