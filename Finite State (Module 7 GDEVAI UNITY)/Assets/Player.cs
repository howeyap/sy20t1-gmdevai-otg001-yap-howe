﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Unit
{

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            StartFiring();
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            StopFiring();
        }
    }
}
