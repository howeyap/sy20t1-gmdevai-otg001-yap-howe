﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField]
    private GameObject player;

    // Update is called once per frame
    void Update()
    {
        Vector3 newPosition = new Vector3(player.transform.position.x, transform.position.y, player.transform.position.z-7);
        transform.position = Vector3.Lerp(transform.position, newPosition, 1f);
    }
}
