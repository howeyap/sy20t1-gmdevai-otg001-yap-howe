﻿
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField]
    float moveSpeed;

    [SerializeField]
    float rotSpeed;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(transform.position.x + Input.GetAxis("Horizontal") * Time.deltaTime * moveSpeed, 
                             this.transform.position.y,
                             transform.position.z + Input.GetAxis("Vertical") * Time.deltaTime * moveSpeed);

        Vector3 mouse = Input.mousePosition;
        Vector3 player = Camera.main.WorldToScreenPoint(transform.position);
        mouse.z = 6;
        Vector3 lookAtGoal = mouse - player;
        float angle = Mathf.Atan2(lookAtGoal.x, lookAtGoal.y) * Mathf.Rad2Deg;
        this.transform.rotation = Quaternion.Slerp(transform.rotation,
                                                  Quaternion.AngleAxis(angle, Vector3.up),
                                                 Time.deltaTime * rotSpeed);

    }
}
