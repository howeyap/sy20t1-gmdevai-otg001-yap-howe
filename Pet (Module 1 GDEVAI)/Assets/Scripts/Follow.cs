﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follow : MonoBehaviour
{
    public Transform goal;

    [SerializeField]
    private float moveSpeed;

    private float slowingSpeed;

    float rotSpeed = 5;

    [SerializeField]
    private float offset;

    void Update()
    {
        Vector3 lookAtGoal = new Vector3(goal.position.x, this.transform.position.y, goal.position.z);

        this.transform.rotation = Quaternion.Slerp(this.transform.rotation,
                                                  Quaternion.LookRotation(lookAtGoal - transform.position),
                                                  Time.deltaTime * rotSpeed);
        if (Vector3.Distance(lookAtGoal, transform.position) > offset)
        {
            slowingSpeed += 2 * Time.deltaTime;
            transform.position = Vector3.Slerp(transform.position, lookAtGoal, Time.deltaTime * moveSpeed);
        }
        else
        {
            slowingSpeed = moveSpeed;
        }
    }
}
